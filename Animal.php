<?php
    class Animal1 {
        public $name = "shaun";
        public $legs = 4;
        public $cold_blooded = "no";

        public function _construct($animal) {
            $this->name = $animal;
            $this->legs = $animal;
            $this->cold_blooded = $animal;
            $this->yell = $animal;
        }
    }

?>

