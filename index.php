<?php
    require_once('Animal.php');
    require_once('ape.php');
    require_once('frog.php');

    
$sheep = new Animal1("shaun");
echo "Name : ".$sheep->name."<br>";
echo "Legs : ".$sheep->legs."<br>";
echo "Cold blooded : ". $sheep->cold_blooded."<br><br>";

$buduk = new frog1("frog");
echo "Name : ".$buduk->name."<br>";
echo "Legs : ".$buduk->legs."<br>";
echo "Cold blooded : ".$buduk->cold_blooded."<br>";
echo "Jump : ".$buduk->jump."<br><br>";

$ape = new ape1("ape");
echo "Name : ".$ape->name."<br>";
echo "Legs: ".$ape->legs."<br>";
echo "Cold blooded : ".$ape->cold_blooded."<br>";
echo "Yell : ".$ape->yell."<br><br>";


?>

